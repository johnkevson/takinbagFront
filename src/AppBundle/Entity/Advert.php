<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Advert
 *
 * @ORM\Table(name="adverts")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdvertRepository")
 */
class Advert
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="NbKilos", type="integer", length=255)
     */
    private $nbKilos;

    /**
     * @var float
     * @ORM\Column(name="price_per_kilo", type="float", length=255, nullable=true   )
     */
    private $pricePerKilo;

    /**
     * @var string
     *    
     * @ORM\Column(name="departure", type="string", length=255)
     */
    private $departure;

    /**
     * @var string
     *@Assert\NotBlank()    
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival", type="string", length=255)
     */
    private $arrival;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_depart", type="datetime")
     */
    private $dateDeparture;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datemaj", type="datetime")
     */
    private $datemaj;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arrive", type="datetime", nullable=true)
     */
    private $dateArrival;


    /**
     * @var string
     *
     * @ORM\Column(name="transport_mode", type="string", length=255)
     */
    private $transportMode;

    /**
     * @var string
     *
     * @ORM\Column(name="restrictions", type="text", nullable=true)
     */
    private $restrictions;

    /**
     * @var int
     *
     * @ORM\Column(name="statut_publication", type="smallint")
     */
    private $statutPublication;
    
    /**
     * @var User
     * One Advert has one author
     * @ORM\ManyToOne(targetEntity="User", inversedBy="adverts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     * @var taker
     * One Advert has one taker
     * @ORM\Column(type="integer", nullable=true)
     */
    private $taker;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\File(mimeTypes={"image/jpeg"})
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="type_annonce", type="string")
     */
     private $typeAnnonce;

    /**
     * @var string
     *
     * @ORM\Column(name="mention", type="string", nullable=true)
     */
    private $mention;

    /**
     * Get mention;
     *
     * @return string
     */
    public function getMention()
    {
        return $this->mention;
    }
    /**
     * Set mention;
     *
     * @return Advert
     */
    public function setMention($mention)
    {
        $this->mention = $mention;
        return $this;
    }


    /**
     * Get type_annonce
     *
     * @return string
     */
    public function getTypeAnnonce()
    {
        return $this->typeAnnonce;
    }
    /**
     * Set type_annonce
     *
     * @return Advert
     */
    public function setTypeAnnonce($typeAnnonce)
    {
        $this->typeAnnonce = $typeAnnonce;
        return $this;
    }
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Set title
     *
     * @return Advert
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbKilos
     *
     * @param int $nbKilos
     *
     * @return Advert
     */
    public function setNbKilos($nbKilos)
    {
        $this->nbKilos = $nbKilos;

        return $this;
    }

    /**
     * Get nbKilos
     *
     * @return int
     */
    public function getNbKilos()
    {
        return $this->nbKilos;
    }

    /**
     * Set pricePerKilo
     *
     * @param float $pricePerKilo
     *
     * @return Advert
     */
    public function setPricePerKilo($pricePerKilo)
    {
        $this->pricePerKilo = $pricePerKilo;

        return $this;
    }

    /**
     * Get pricePerKilo
     *
     * @return float
     */
    public function getPricePerKilo()
    {
        return $this->pricePerKilo;
    }

    /**
     * Set departure
     *
     * @param string $departure
     *
     * @return Advert
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return string
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param string $arrival
     *
     * @return Advert
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return string
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set dateDeparture
     *
     * @param string $dateDeparture
     *
     * @return Advert
     */
    public function setDateDeparture($dateDeparture)
    {
        $this->dateDeparture = $dateDeparture;

        return $this;
    }

    /**
     * Get dateDeparture
     *
     * @return \DateTime
     */
    public function getDateDeparture()
    {
        return $this->dateDeparture;
    }

    /**
     * Set dateArrival
     *
     * @param \DateTime $dateArrival
     *
     * @return Advert
     */
    public function setDateArrival($dateArrival)
    {
        $this->dateArrival = $dateArrival;

        return $this;
    }
    /**
     * Get datemaj
     *
     * @return \DateTime
     */
    public function getDatemaj()
    {
        return $this->datemaj;
    }

    /**
     * Set datemaj
     *
     * @param \DateTime $datemaj
     *
     * @return Advert
     */
    public function setDatemaj($datemaj)
    {
        $this->datemaj = $datemaj;

        return $this;
    }
    /** 
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Advert
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get dateArrival
     *
     * @return \DateTime
     */
    public function getDateArrival()
    {
        return $this->dateArrival;
    }    


    /**
     * Set transportMode
     *
     * @param string $transportMode
     *
     * @return Advert
     */
    public function setTransportMode($transportMode)
    {
        $this->transportMode = $transportMode;

        return $this;
    }

    /**
     * Get transportMode
     *
     * @return string
     */
    public function getTransportMode()
    {
        return $this->transportMode;
    }

    /**
     * Set restrictions
     *
     * @param string $restrictions
     *
     * @return Advert
     */
    public function setRestrictions($restrictions)
    {
        $this->restrictions = $restrictions;

        return $this;
    }

    /**
     * Get restrictions
     *
     * @return string
     */
    public function getRestrictions()
    {
        return $this->restrictions;
    }

    /**
     * Set statutPublication
     *
     * @param integer $statutPublication
     *
     * @return Advert
     */
    public function setStatutPublication($statutPublication)
    {
        $this->statutPublication = $statutPublication;

        return $this;
    }

    /**
     * Get statutPublication
     *
     * @return int
     */
    public function getStatutPublication()
    {
        return $this->statutPublication;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Advert
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    public function setTaker($taker)
    {
        $this->taker= $taker;
        return $this;
    }
    public function getTaker()
    {
        return $this->taker;
    }
    /**
     * Is the given User the author of this Post?
     *
     * @return bool
     */
    public function isAuthor(User $user = null)
    {
        return $user && $user->getEmail() === $this->getUser()->getEmail();
    }
}
