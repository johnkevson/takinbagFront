<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Rib
 *
 * @ORM\Table(name="rib")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RibRepository")
 */

class Rib
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Iban(
     *     message="Iban invalide"
     *  )   
     * @ORM\Column(name="iban", type="string", length=255, unique=true)
     */
    private $iban;

    /**
     * @var string
     * @Assert\Bic(
     *   message="Bic invalide"  
     *  )   
     * @ORM\Column(name="bic", type="string", length=255)
     */
    private $bic;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_titulaire", type="string", length=255)
     */
    private $nomTitulaire;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var int
     *
     * @ORM\Column(name="code_postal", type="integer")
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var User
     * One user has one Rib
     * @ORM\OneToOne(targetEntity="User", inversedBy="rib")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return Rib
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return Rib
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set nomTitulaire
     *
     * @param string $nomTitulaire
     *
     * @return Rib
     */
    public function setNomTitulaire($nomTitulaire)
    {
        $this->nomTitulaire = $nomTitulaire;

        return $this;
    }

    /**
     * Get nomTitulaire
     *
     * @return string
     */
    public function getNomTitulaire()
    {
        return $this->nomTitulaire;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Rib
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Rib
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return int
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Rib
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }
    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Rib
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

