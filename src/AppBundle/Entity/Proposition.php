<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proposition
 *
 * @ORM\Table(name="proposition")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropositionRepository")
 */
class Proposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_annonce_giver", type="integer")
     */
    private $idAnnonceGiver;

    /**
     * @var int
     *
     * @ORM\Column(name="id_annonce_taker", type="integer")
     */
    private $idAnnonceTaker;
    /**
     * @var int
     *
     * @ORM\Column(name="poids_requis", type="integer")
     */
    private $poidsRequis;

    /**
     * @var int
     *
     * @ORM\Column(name="type_proposition", type="smallint")
     */
    private $typeProposition;
    /**
     * @var int
     *
     * @ORM\Column(name="id_giver", type="integer")
     */
    private $idGiver;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idAnnonceGiver
     *
     * @param integer $idAnnonceGiver
     *
     * @return Proposition
     */
    public function setIdAnnonceGiver($idAnnonceGiver)
    {
        $this->idAnnonceGiver = $idAnnonceGiver;

        return $this;
    }

    /**
     * Get idAnnonceGiver
     *
     * @return int
     */
    public function getIdAnnonceGiver()
    {
        return $this->idAnnonceGiver;
    }
    /**
     * Set idGiver
     *
     * @param integer $idGiver
     *
     * @return Proposition
     */
    public function setidGiver($idGiver)
    {
        $this->idGiver = $idGiver;

        return $this;
    }

    /**
     * Get idGiver
     *
     * @return int
     */
    public function getIdGiver()
    {
        return $this->idGiver;
    }

    /**
     * Set idAnnonceTaker
     *
     * @param integer $idAnnonceTaker
     *
     * @return Proposition
     */
    public function setIdAnnonceTaker($idAnnonceTaker)
    {
        $this->idAnnonceTaker = $idAnnonceTaker;

        return $this;
    }
    /**
     * Get poidsRequis
     *
     * @return int
     */
    public function getPoidsRequis()
    {
        return $this->poidsRequis;
    }

    /**
     * Set poidsRequis
     *
     * @param integer $poidsRequis
     *
     * @return Proposition
     */
    public function setPoidsRequis($poidsRequis)
    {
        $this->poidsRequis = $poidsRequis;

        return $this;
    }

    /**
     * Get idAnnonceTaker
     *
     * @return int
     */
    public function getIdAnnonceTaker()
    {
        return $this->idAnnonceTaker;
    }

    /**
     * Set typeProposition
     *
     * @param integer $typeProposition
     *
     * @return Proposition
     */
    public function setTypeProposition($typeProposition)
    {
        $this->typeProposition = $typeProposition;

        return $this;
    }

    /**
     * Get typeProposition
     *
     * @return int
     */
    public function getTypeProposition()
    {
        return $this->typeProposition;
    }
}

