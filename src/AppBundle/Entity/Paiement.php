<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Paiement
 *
 * @ORM\Table(name="paiement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaiementRepository")
 */
class Paiement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num_paiement", type="string")
     */
    private $numPaiement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_paiement", type="datetime")
     */
    private $datePaiement;

    /**
     * @var string
     *
     * @ORM\Column(name="destinataire_paiement", type="string", length=255)
     */
    private $destinatairePaiement;
    /**
     * @var string
     *
     * @ORM\Column(name="auteur_paiement", type="string", length=255)
     */
    private $auteurPaiement;


    /**
     * @var float
     *
     * @ORM\Column(name="montant_initial", type="float")
     */
    private $montantInitial;

    /**
     * @var float
     *
     * @ORM\Column(name="montant_calcule", type="float")
     */
    private $montantCalcule;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_maj", type="datetime")
     */
    private $dateMaj;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numPaiement
     *
     * @param string $numPaiement
     *
     * @return Paiement
     */
    public function setNumPaiement($numPaiement)
    {
        $this->numPaiement = $numPaiement;

        return $this;
    }

    /**
     * Get numPaiement
     *
     * @return int
     */
    public function getNumPaiement()
    {
        return $this->numPaiement;
    }

    /**
     * Set datePaiement
     *
     * @param \DateTime $datePaiement
     *
     * @return Paiement
     */
    public function setDatePaiement($datePaiement)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * Get datePaiement
     *
     * @return \DateTime
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }

    /**
     * Set destinaiairePaiement
     *
     * @param string $destinatairePaiement
     *
     * @return Paiement
     */
    public function setDestinatairePaiement($destinatairePaiement)
    {
        $this->destinatairePaiement = $destinatairePaiement;

        return $this;
    }

    /**
     * Get auteurPaiement
     *
     * @return string
     */
    public function getAuteurPaiement()
    {
        return $this->auteurPaiement;
    }
    /**
     * Set auteurPaiement
     *
     * @param string $auteurPaiement
     *
     * @return Paiement
     */
    public function setAuteurPaiement($auteurPaiement)
    {
        $this->auteurPaiement = $auteurPaiement;

        return $this;
    }

    /**
     * Get destinatairePaiement
     *
     * @return string
     */
    public function getDestinatairePaiement()
    {
        return $this->destinatairePaiement;
    }

    /**
     * Set montantInitial
     *
     * @param float $montantInitial
     *
     * @return Paiement
     */
    public function setMontantInitial($montantInitial)
    {
        $this->montantInitial = $montantInitial;

        return $this;
    }

    /**
     * Get montantInitial
     *
     * @return int
     */
    public function getMontantInitial()
    {
        return $this->montantInitial;
    }

    /**
     * Set montantCalcule
     *
     * @param float $montantCalcule
     *
     * @return Paiement
     */
    public function setMontantCalcule($montantCalcule)
    {
        $this->montantCalcule = $montantCalcule;

        return $this;
    }

    /**
     * Get montantCalcule
     *
     * @return int
     */
    public function getMontantCalcule()
    {
        return $this->montantCalcule;
    }

    /**
     * Set dateMaj
     *
     * @param \DateTime $dateMaj
     *
     * @return Paiement
     */
    public function setDateMaj($dateMaj)
    {
        $this->dateMaj = $dateMaj;

        return $this;
    }

    /**
     * Get dateMaj
     *
     * @return \DateTime
     */
    public function getDateMaj()
    {
        return $this->dateMaj;
    }
}

