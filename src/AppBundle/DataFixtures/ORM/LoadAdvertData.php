<?php



namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Advert;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadAdvertData extends AbstractFixture
    implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // Creation de trois annonces pour transport
        $ad1 = new Advert();
        $ad1->setCreated(new \DateTime());
        $ad1->setDatemaj(new \DateTime());
        $user = $this->getReference('user-one');
        $ad1->setUser($user);

        $ad1->setArrival('Paris, France');
        $da = new \Datetime('2018-06-19');
        $ad1->setDateArrival($da);

        $ad1->setDeparture('New York, USA');
        $dd = new \Datetime('2018-05-15');
        $ad1->setDateDeparture($dd);


        $ad1->setNbKilos(49);
        $ad1->setPricePerKilo(7.5);
        $ad1->setStatutPublication(1);
        $ad1->setTitle('Vacances à Paris');
        $ad1->setTransportMode(4);
        $ad1->setTypeAnnonce(1);


        // Creation de trois annonces pour transport
        $ad2 = new Advert();
        $ad2->setCreated(new \DateTime());
        $ad2->setDatemaj(new \DateTime());
        $user = $this->getReference('user-one');
        $ad2->setUser($user);
        $ad2->setArrival('Paris, France');

        $ad2->setDateArrival($da);

        $ad2->setDeparture('New York, USA');
        $ad2->setDateDeparture($dd);

        $ad2->setNbKilos(49);
        $ad2->setPricePerKilo(7.5);
        $ad2->setStatutPublication(1);
        $ad2->setTitle('Vacances à Paris');
        $ad2->setTransportMode(4);
        $ad2->setTypeAnnonce(1);


        // Creation de trois annonces pour transport
        $ad3 = new Advert();
        $ad3->setCreated(new \DateTime());
        $ad3->setDatemaj(new \DateTime());
        $user = $this->getReference('user-one');
        $ad3->setUser($user);
        $ad3->setArrival('Paris, France');
        $ad3->setDateArrival($da);
        $ad3->setDeparture('New York, USA');
        $ad3->setDateDeparture($dd);

        $ad3->setNbKilos(49);
        $ad3->setPricePerKilo(7.5);
        $ad3->setStatutPublication(1);
        $ad3->setTitle('Vacances à Paris');
        $ad3->setTransportMode(4);
        $ad3->setTypeAnnonce(1);



        // Creation de trois annonces pour transport
        $ad4 = new Advert();
        $ad4->setCreated(new \DateTime());
        $ad4->setDatemaj(new \DateTime());
        $user = $this->getReference('user-two');
        $ad4->setUser($user);
        $ad4->setArrival('Paris, France');
        $ad4->setDateArrival($da);
        $ad4->setDeparture('New York, USA');
        $ad4->setDateDeparture($dd);

        $ad4->setNbKilos(49);
        $ad4->setStatutPublication(1);
        $ad4->setTitle('Vacances à Paris');
        $ad4->setTransportMode(4);
        $ad4->setTypeAnnonce(2);


        // Creation de trois annonces pour transport
        $ad5 = new Advert();
        $ad5->setCreated(new \DateTime());
        $ad5->setDatemaj(new \DateTime());
        $user = $this->getReference('user-two');
        $ad5->setUser($user);
        $ad5->setArrival('Paris, France');
        $ad5->setDateArrival($da);
        $ad5->setDeparture('New York, USA');
        $ad5->setDateDeparture($dd);

        $ad5->setNbKilos(49);
        $ad5->setPricePerKilo(7.5);
        $ad5->setStatutPublication(1);
        $ad5->setTitle('Vacances à Paris');
        $ad5->setTransportMode(4);
        $ad5->setTypeAnnonce(1);


        // Creation de trois annonces pour transport
        $ad6 = new Advert();
        $ad6->setCreated(new \DateTime());
        $ad6->setDatemaj(new \DateTime());
        $user = $this->getReference('user-two');
        $ad6->setUser($user);
        $ad6->setArrival('Paris, France');
        $ad6->setDateArrival($da);
        $ad6->setDeparture('New York, USA');
        $ad6->setDateDeparture($dd);

        $ad6->setNbKilos(49);
        $ad6->setStatutPublication(1);
        $ad6->setTitle('Vacances à Paris');
        $ad6->setTransportMode(4);
        $ad6->setTypeAnnonce(2);


        // Creation de trois annonces pour transport
        $ad7 = new Advert();
        $ad7->setCreated(new \DateTime());
        $ad7->setDatemaj(new \DateTime());
        $user = $this->getReference('user-two');
        $ad7->setUser($user);
        $ad7->setArrival('Paris, France');
        $ad7->setDateArrival($da);
        $ad7->setDeparture('New York, USA');
        $ad7->setDateDeparture($dd);

        $ad7->setNbKilos(49);
        $ad7->setStatutPublication(1);
        $ad7->setTitle('Vacances à Paris');
        $ad7->setTransportMode(4);
        $ad7->setTypeAnnonce(2);


        // Creation de trois annonces pour transport
        $ad8 = new Advert();
        $ad8->setCreated(new \DateTime());
        $ad8->setDatemaj(new \DateTime());
        $user = $this->getReference('user-two');
        $ad8->setUser($user);
        $ad8->setArrival('Paris, France');
        $ad8->setDateArrival($da);
        $ad8->setDeparture('New York, USA');
        $ad8->setDateDeparture($dd);

        $ad8->setNbKilos(49);
        $ad8->setStatutPublication(1);
        $ad8->setTitle('Vacances à Paris');
        $ad8->setTransportMode(4);
        $ad8->setTypeAnnonce(2);


        // Creation de trois annonces pour transport
        $ad9 = new Advert();
        $ad9->setCreated(new \DateTime());
        $ad9->setDatemaj(new \DateTime());
        $user = $this->getReference('user-one');
        $ad9->setUser($user);
        $ad9->setArrival('Paris, France');
        $ad9->setDateArrival($da);
        $ad9->setDeparture('New York, USA');
        $ad9->setDateDeparture($dd);

        $ad9->setNbKilos(49);
        $ad9->setPricePerKilo(7.5);;
        $ad9->setStatutPublication(1);
        $ad9->setTitle('Vacances à Paris');
        $ad9->setTransportMode(4);
        $ad9->setTypeAnnonce(1);

        $manager->persist($ad1);
        $manager->persist($ad2);
        $manager->persist($ad3);
        $manager->persist($ad4);
        $manager->persist($ad5);
        $manager->persist($ad6);
        $manager->persist($ad7);
        $manager->persist($ad8);
        $manager->persist($ad9);

        $manager->flush();

    }

    /**
     * Get the order of this fixture
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }

}