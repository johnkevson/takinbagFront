<?php


namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Entity\User;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class LoadUserData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        // Creation de l'utilisateur zicklr
        $user = new User();
        $user->setName("Kevson");
        $user->setLastname("John");
        $pwd = $this->encoder->encodePassword($user, 'DeVrij75');
        $user->setPassword($pwd);
        $user->setEmail('johnkevson@gmail.com');
        $user->setUsername('Zicklr');
        $user->setRole(0);
        $user->setStatus(1);
        $user->setDatemaj(new \DateTime());
        $user->setCreated(new \DateTime());
        $user->setPhone('0619524635');

        $manager->persist($user);
        // Creation de l'utilisateur zicklr
        $user2 = new User();
        $user2->setName("Motto");
        $user2->setLastname("Jeremies");
        $pwd = $this->encoder->encodePassword($user2, 'X4T4593Sw');
        $user2->setPassword($pwd);
        $user2->setEmail('minkol_kevin@yahoo.fr');
        $user2->setUsername('Motto');
        $user2->setRole(0);
        $user2->setStatus(1);
        $user2->setDatemaj(new \DateTime());
        $user2->setCreated(new \DateTime());
        $user2->setPhone('0618850203');
        $manager->persist($user2);

        $manager->flush();
        $this->addReference('user-two', $user2);
        $this->addReference('user-one', $user);

    }

    /**
     * Get the order of this fixture
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

}