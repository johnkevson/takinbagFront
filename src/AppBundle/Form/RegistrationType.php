<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Nom')))
                ->add('lastname',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Prénom')))
                ->add('email', EmailType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Adresse électronique')))
                ->add('phone', TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Numéro de téléphone')))
                ->add('username',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Nom d\'utilisateur')))
                ->add('password',PasswordType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input uk-form-width-medium','placeholder'=>'Mot de passe')))
                ->add('valider', SubmitType::class, array('attr'=>array('class'=>'uk-input uk-button uk-button-violet uk-form-width-medium','value'=>'Enregistrer')));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'registration';
    }
    

}
