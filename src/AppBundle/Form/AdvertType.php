<?php

namespace AppBundle\Form;


use Proxies\__CG__\AppBundle\Entity\Advert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class AdvertType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nbKilos',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input ','placeholder'=>'Nombre de kilos')))
                ->add('title',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input ','placeholder'=>'Titre de votre annonce')))
                ->add('pricePerKilo',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input','placeholder'=>'Prix au kilos'),'required'=>false))
                ->add('departure',TextType::class, array('label'=>'', 'attr'=>array('class'=>'uk-input','placeholder'=>'Lieu de départ')))
                ->add('arrival', TextType::class, array('label'=>'Lieu d\'arrivée', 'attr'=>array('class'=>'uk-input', 'placeholder'=>'Lieu de d\'arrivée')))
                ->add('typeAnnonce', ChoiceType::class, array('label'=>'', 'choices'=>array('Type d\'annonce'=>'null', 'Envoyer un colis'=>'1', 'Transporter un colis'=>'2'), 'attr'=>array('class'=>'uk-select')))
                ->add('dateDeparture', DateType::class, array('widget'=>'single_text','format'=>'yyyy-MM-dd','html5'=>false,'attr'=>array('class'=>'uk-input')))
                ->add('dateArrival', DateType::class, array('widget'=>'single_text','format'=>'yyyy-MM-dd','html5'=>false,'attr'=>array('class'=>'uk-input')))
                ->add('transportMode', ChoiceType::class, array('label'=>'Lieu de départ', 'attr'=>array('class'=>'uk-select'), 'choices'=>array('Choisir un mode de déplacement'=>'null','Avion'=>'1', 'Bateau'=>'2','Train'=>'3','Voiture'=>'4')))
                ->add('restrictions',TextareaType::class, array('label'=>'Précisions', 'attr'=>array('class'=>'uk-input')))
                ->add('image',FileType::class, array(
                        'label'=>false,
                        'data_class'=>null
                    )
                );
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>'AppBundle\Entity\Advert'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_advert';
    }


}
