<?php

namespace AppBundle\Controller;


use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('@App/Customer/Security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function loginCheckAction()
    {

    }
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }
    
    /**
     * @Route("/password-recovery", name="password_recovery")
     */
    public function passwordRecovery(){
        $em = $this->getDoctrine()->getManager();
        $request = Request::createFromGlobals();
        $mailer = $this->container->get('mailer');
        if ($request->isMethod('POST')) {
            // l'email est bien en base ? Si oui, on envoie un email
            $email = $request->request->get('email');
            $userObject = $em->getRepository('AppBundle:User')->findBy(
                array(
                    'email'=>$email
                )
            );
           if (!empty($userObject)) {
               // j'encode mon email et je un nombre aléetoire de 3 chiffres
               $emailEncoded = base64_encode($email).rand(1000, 9999);
               // On envoie un email au type pour qu'il complète son inscription
               $message = new \Swift_Message('Support Takinbag - Lien de réinitialisation  de  votre mot de passe');
               $message->setFrom('support@takinbag.com')->setTo($email)
                   ->setBody(
                       $this->renderView(
                       // app/Resources/views/Emails/registration.html.twig
                           'AppBundle:Emails:reinit_password.html.twig',
                           array('email' => $emailEncoded)
                       ),
                       'text/html'
                   )
               ;
               $mailer->send($message);
               // flag succes
               $success = 1;
               return $this->render('@App/Customer/Security/passwordRecovery.html.twig', array('success'=>$success)); 
           }
            
        }
        $success = 0;
        return $this->render('@App/Customer/Security/passwordRecovery.html.twig', array('success'=>$success));
    }
    /**
     * @Route("/change-password/{email}", name="change_password")
     */
    public function changePassword($email)
    {
        $request = Request::createFromGlobals();
        $em = $this->getDoctrine()->getManager();
        /** On retrouve le mail d'origine */
        $length = strlen($email);
        
        $emailCoded = substr($email,0,$length-4);
        $mail =  base64_decode($emailCoded);

        if ($request->isMethod('POST')) {
 
            $foundUser = $em->getRepository('AppBundle:User')->findBy(
                array(
                    'email' => $mail
                )
            );

            $userObject = $foundUser[0];    
            $pass = $request->request->get('password');
                 
                 // On met à jour le mot de passe dans la base
             // On met à jour le mot de passe lié à cet email
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($userObject, $pass);
            $userObject->setPassword($pwd);
            $em->persist($userObject);
            $em->flush();
            $changedPass = 1;
            return $this->render('@App/Customer/Security/passwordRecovery_bis.html.twig', array('email'=>$email, 'success'=>$changedPass));
        }
        $changedPass = 0;
        return $this->render('@App/Customer/Security/passwordRecovery_bis.html.twig', array('email'=>$email, 'success'=>$changedPass));
    }
}