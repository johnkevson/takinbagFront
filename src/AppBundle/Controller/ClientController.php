<?php
namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ClientController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction()
    {
        /** On recupère les colis envoyés par le user connecté */
        $em = $this->getDoctrine()->getManager();
        $userConnecte = $this->getUser()->getId();
        $adverts_envoyes = $em->getRepository('AppBundle:Advert')->findBy(array(
           'statutPublication'=>1,
            'typeAnnonce'=>1,
            'user'=>$userConnecte
        ));
        $adverts_transportees = $em->getRepository('AppBundle:Advert')->findBy(array(
            'statutPublication'=>1,
            'typeAnnonce'=>2,
            'user'=>$userConnecte
        ));
        return $this->render('AppBundle:Customer:account_customer.html.twig', array(
            'liste_adverts_send'=>$adverts_envoyes,
            'liste_adverts_taken'=>$adverts_transportees
        ));
    }
    
    /**
     * @Route("/verifierEmail/{email}/{verifie}", name="verifierEmail")
     */
    public function verifierEmailAction($email, $verifie)
    {
        /*** Module de vérification de l'adresse email
         * Si c'est un email, on anvoie un email et on retourne le c
         ***/
        $mailer = $this->container->get('mailer');
        
        if($verifie==0){
            // On envoie un email au type pour qu'il complète son inscription
            $message = new \Swift_Message('Support Takinbag - Lien de vérification de votre adresse mail');
            $message->setFrom('support@takinbag.com')->setTo($email)
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        '@App/Emails/checkEmail.html.twig',
                        array('email' => $email, 'verifie'=>$verifie)
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
        }
        // On met a jour le statut en ligne du profil
        if($verifie==1){
            $em = $this->getDoctrine()->getManager();
            $this->getUser()->setStatus($verifie);
            $em->flush();
        }
        return $this->render('AppBundle:Customer:verifierEmail.html.twig'
            );
    }
}