<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
            'users' => $users,
        ));
    }
    /**
     * Implements user's notes.
     *
     * @Route("/note", name="user_notes")
     * @Method("POST")
     */
    public function majNotesUserAction(Request $request)
    {
        /**
         * On calcule la note et on renvoie le nombre d'étoile en fonction de la note
         * 1-/ On récupère la note actuelle
         * 2-/ On ajoute à la note courante la note actuelle
         * 3-/ On divise par 5 et si le resultat est <  5 => 1 etoile, resultat > 5 et < 10 => 2 etoiles,
         * >10 et < 25 => 3 etoiles , > 25 et < 35 => 4 etoiles , > 45 => 5 etoiles
          *
         **/
        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository('AppBundle:Advert')->find($request->get('advertId'));
        $myNote = $request->get('noteDonnee');
        if(empty($advert->getUser()->getNote())){
            $advert->getUser()->setNote($myNote);
            $em->flush();
            return new JsonResponse(array('note'=>1));
        }else{
            $noteCourante = $advert->getUser()->getNote();
            $noteCourante = $noteCourante+$myNote;
            $advert->getUser()->setNote($noteCourante);
            $noteCourante = $noteCourante/5;
            $em->flush();
            if($noteCourante <= 5){
                // Une etoile
                return new JsonResponse(array('note'=>1));
            }
            if($noteCourante > 5 && $noteCourante <= 10){
                // Deux etoiles
                return new JsonResponse(array('note'=>2));
            }
            if($noteCourante > 10 && $noteCourante <= 25){
                // Trois etoiles
                return new JsonResponse(array('note'=>3)) ;
            }
            if($noteCourante > 25 && $noteCourante <= 45){
                // Quatre etoiles
                return new JsonResponse(array('note'=>4));
            }
            if($noteCourante > 45 ){
                // Quatre etoiles
                return new JsonResponse(array('note'=>5));
            }

        }
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        //$deleteForm = $this->createDeleteForm($user);

        return $this->redirectToRoute('user_show', array(
            'id' => $user->getId()
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {

        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('AppBundle:user:edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/{id}/upload/", name="user_upload_picture")
     * @Method({"GET", "POST"})
     */
    public function uploadUserAvatarAction($id, Request $request){
        if($request->getMethod()== 'POST'){
            $myAvatar = $request->files->get('file');
            if(!in_array($myAvatar->guessExtension(), array('jpeg', 'png')) && $myAvatar->isValid()){
                return $this->addFlash('warning', "Le fichier uploadé n'a pas le bon format");
            }else{
                /* On traite le fichier et on garde les infos essentielles dans la bdd */
                $em = $this->getDoctrine()->getManager();
                $my_user = $em->getRepository('AppBundle\Entity\User')->find($id);
                if(!empty($my_user)){
                    /** On déplace l'image et on ajoute le chemin vers l'avatar **/
                    $filename = md5(uniqid()).'.'.$myAvatar->guessExtension();
                    $fileAbsolutePath = $this->get('service_container')->getParameter('image_directory').DIRECTORY_SEPARATOR.$filename;
                    if(!empty($my_user->getAvatar()) && file_exists(dirname($this->get('kernel')->getRootDir()).'/web/'.$my_user->getAvatar())){
                        unlink(dirname($this->get('kernel')->getRootDir()).'/web/'.$my_user->getAvatar());
                    }
                    if($myAvatar->move($this->get('service_container')->getParameter('image_directory'),$filename)){
                        $my_user->setAvatar($this->get('service_container')->getParameter('image_directory').DIRECTORY_SEPARATOR.$filename);
                    }
                    $em->flush();
                    return new JsonResponse(array('chemin'=>$fileAbsolutePath, 'message'=>'Photo ajoutée avec succès'), 200);
                }
            }
        }
        return null;
    }


    /**
     * Deletes a user entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
