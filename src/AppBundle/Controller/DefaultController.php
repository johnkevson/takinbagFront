<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Rib;
use AppBundle\Form\RegistrationType;
use AppBundle\Form\RibType;
use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        if($request->isXmlHttpRequest()){
            $deja_vu = $request->cookies->has('takinbag_first_visit');
            if(!$deja_vu){
                $cookie_info = array(
                    'name' => 'takinbag_first_visit', // Nom du cookie
                    'value' => '12345', // Valeur du cookie
                    'time' => time() + 3600 * 24 * 7 // Durée de vie du cookie
                );
                // Création du cookie
                $cookie = new Cookie($cookie_info['name'], $cookie_info['value'], $cookie_info['time']);
                $response = new Response();
                $response->headers->setCookie($cookie);
                $response->send();
                return $this->render('default/index.html.twig',array('deja_vu'=>$deja_vu));
            }
            return $this->render('default/index.html.twig',array('deja_vu'=>$deja_vu));

        }
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR
        ));
    }

    /**
     * @Route("/about/us", name="about")
     */
    public function aboutAction()
    {
        return $this->render('AppBundle:partials:about.html.twig');
    }
    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $mailer = $this->container->get('mailer');
        $formFactory = Forms::createFormFactory();
        $form = $formFactory->createBuilder()
            ->add('subject', TextType::class)
            ->add('email', EmailType::class)
            ->add('message', TextareaType::class)
            ->getForm();
        $form->handleRequest();
        if($form->isValid() && $form->isSubmitted()){
            //Traitement on envoi le mail à takinbagsupport
            // On envoie un email au type pour qu'il complète son inscription
            $flashbag = $this->get('session')->getFlashBag();
            $values = $request->get('form');
            if(!empty($values)){
                $message = new \Swift_Message($values['subject']);
                $message->setFrom($values['email'])
                    ->setTo($this->container->getParameter('email_support'))
                    ->setBody($values['message']);
                $mailer->send($message);
                // Retrieve flashbag from the controller
                // replace this example code with whatever you need
                return $this->render('AppBundle:contact:index.html.twig', array(
                    'form_contact'=>$form->createView(),
                    'notification'=>1
                ));

            } else {
                $flashbag->add('error','une erreur s\'est produite pendant l\'envoi de votre message');
                // replace this example code with whatever you need
                return $this->render('AppBundle:contact:index.html.twig', array(
                    'form_contact'=>$form->createView(),
                    'notification'=>-1
                ));
            }

        }
        return $this->render('AppBundle:contact:index.html.twig', array('form_contact'=>$form->createView()));
    }
    /**
     * @Route("/paramsCookies", name="cookies")
     */
    public function paramsCookiesAction()
    {
        return $this->render('AppBundle:partials:cookies.html.twig');
    }
    /**
     * @Route("/putCookie", name="put_cookie")
     */
    public function putCookieAction(Request $request)
    {
        $response = new Response();
        $response->headers->setCookie(new Cookie('foo', 'bar'));
        $response->headers->set('set-cookie', 'takinbag=bar', false);
        return $this->redirect('/');
    }
    /**
     * @Route("/legal-information", name="legal_info")
     */
    public function legalInformationAction()
    {
        return $this->render('AppBundle:partials:legal.html.twig');
    }
    /**
     * @Route("/conditions-generales", name="cgv")
     */
    public function cgvAction()
    {
        return $this->render('AppBundle:partials:cgv.html.twig');
    }
    /**
     * @Route("/confidentialite", name="confidentialite")
     */
    public function confidentialiteAction()
    {
        return $this->render('AppBundle:partials:confidentialite.html.twig');
    }
    /**
     * @Route("/suscribe", name="suscribe")
     */
    public function suscribeAction(Request $request, \Swift_Mailer $mailer)
    {
        $registration = new User();
        $em   = $this->getDoctrine()->getManager();
        // On crée le formulaire et on l'envoie dans le template
        $userForm = $this->createForm(RegistrationType::class, $registration, ['action'=>$this->generateUrl('suscribe'),'method'=>'POST']);
        $userForm->handleRequest($request);
        if($userForm->isSubmitted() && $userForm->isValid()){
            $userNew = $userForm->getData();
            $monEmail = $userNew->getEmail();
            
            // si l'email n'existe pas encore, on peut continuer
            $alreadyExistantEmail = $em->getRepository('AppBundle:User')->findBy(
                array('email'=>$monEmail)
            );
            if(!$alreadyExistantEmail){
                
                $registration->setCreated(new \DateTime());
                $registration->setDatemaj(new \DateTime());
                $registration->setStatus(0);
                $registration->setEmail($userNew->getEmail());
                $registration->setName($userNew->getName());
                $registration->setLastname($userNew->getLastName());
                $registration->setUsername($userNew->getUsername());
                $registration->setRole(0);
                
                // On encrypte le mot de passe 
                
                $pwd = $userNew->getPassword();
                $encoder=$this->container->get('security.password_encoder');
                $pwd = $encoder->encodePassword($userNew, $pwd);
                $registration->setPassword($pwd);
                $em->persist($registration);
                $em->flush();
                
                // On encrypte l'id
                
                $myId = base64_encode($userNew->getId());
                $hashedLogin  = uniqid().'-'.$myId;

                // On envoie un email au type pour qu'il complète son inscription
                $message = new \Swift_Message('Confirmation d\'inscription');
                $message->setFrom('support@takinbag.com')->setTo($userNew->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'AppBundle:Emails:registration.html.twig',
                            array('name' => $userNew->getUsername(), 'hashedLogin'=>$hashedLogin)
                        ),
                        'text/html'
                    )
                ;
                $mailer->send($message);

                // replace this example code with whatever you need
                return $this->render('AppBundle:Customer:registration.html.twig', array(
                    'formUser'=>$userForm->createView(),
                    'insc_reussie'=>'1'
                )); 
            }
        }
        // replace this example code with whatever you need
        return $this->render('AppBundle:Customer:registration.html.twig', array(
            'formUser'=>$userForm->createView()
        ));
    }
    /**
     * @Route("/login/activation/{hash_login}", name="login_activation")
     */
    public function loginActivation($hash_login)
    {
        return $this->redirectToRoute('user_login', array('hash_login'=>$hash_login,'first_connexion'=>1));
    }
    /**
     * @Route("/user/retrieve/{hash_login}/{first_connexion}", name="user_login")
     */
    public function userLogin($hash_login, $first_connexion)
    {
        $myId = explode('-', $hash_login);
        $accountId = base64_decode($myId[1]);
        return $this->render('@App/Customer/Security/login.html.twig', array(
            'id'=>$accountId
        ));
    }
    /**
     * On affiche les blocs qui correspondent aux annonces envoyées
     * @Route("/cote-expediteur", name="cote_expediteur")
     */
    public function coteExpediteurAction($id){
            $em = $this->getDoctrine()->getManager();
//            $adverts = $em->getRepository('Advert')->findBy('user')
           return $this->render('AppBundle:Customer:space-giver.html.twig');
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/invoices-done", name="invoices_done")
     */
    public function invoicesDoneAction(Request $request)
    {
        // On récupère tous les paiements faits par le client grace à son email
        $myEmail = $this->getUser()->getEmail();
        $em = $this->getDoctrine()->getManager();
        $ribInfos = $em->getRepository('AppBundle:Rib')->findOneBy(
            array('user'=>$this->getUser())
        );
        if(!empty($ribInfos)){
            $ribForm = $this->createForm(RibType::class, $ribInfos, ['action'=>$this->generateUrl('invoices_done'),'method'=>'POST']);
        }else{
            $rib = new Rib();
            $ribForm = $this->createForm(RibType::class, $rib, ['action'=>$this->generateUrl('invoices_done'),'method'=>'POST']);
        }
        if(!empty($myEmail)){
            $stripe = array(
                'secret_key' => $this->getParameter('secret_stripe_key'),
                'publishable_key' => $this->getParameter('public_stripe_key')
            );

            Stripe::setApiKey($stripe['secret_key']);
            $allCharges = Charge::all();
            $listCharges = $allCharges->__toArray()['data'];
            $tabCharges = array();
            /*** On récupère toutes les transactions du client connecté ***/
            foreach($listCharges as $k => $charge){
                    /* On ne récupère que les paiements dont l'email connecté est à l'origine */
                    if($listCharges[$k]['source']['name'] == $this->getUser()->getEmail()){
                        $listCharges[$k]['created'] = date('d/m/Y', $listCharges[$k]['created']);
                        $tabCharges[$k]['date'] = $listCharges[$k]['created'];
                        $tabCharges[$k]['id'] = $listCharges[$k]['id'];
                        $tabCharges[$k]['montant'] = $listCharges[$k]['amount']/100;
                        $tabCharges[$k]['email'] = $listCharges[$k]['source']['name'];
                        $tabCharges[$k]['type'] = $listCharges[$k]['source']['brand'];
                        $tabCharges[$k]['status'] = $listCharges[$k]['status'];
                    }
            }
            $ribForm->handleRequest($request);
            if($ribForm->isSubmitted() && $ribForm->isValid()){
                /** On ajoute le rib s'il n'existe pas encore pour cet utilisateur  */
                $datasRib = $ribForm->getData();
                // Si le rib existe déjà, on ne rajoute que les champs qui ont changé
                if(!empty($ribInfos)){
                    if($ribInfos->getIban() !== $datasRib->getIban()){
                        $ribInfos->setIban($datasRib->getIban());   
                    }
                    if($ribInfos->getAdresse() !== $datasRib->getAdresse()) {
                        $ribInfos->setAdresse($datasRib->getAdresse());
                    }
                    if($ribInfos->getBic() !== $datasRib->getBic()) {
                        $ribInfos->setBic($datasRib->getBic());
                    }
                    if($ribInfos->getNomTitulaire() !== $datasRib->getNomTitulaire()) {
                        $ribInfos->setNomTitulaire($datasRib->getNomTitulaire());
                    }
                    if($ribInfos->getCodePostal() !== $datasRib->getCodePostal()) {
                        $ribInfos->setCodePostal($datasRib->getCodePostal());
                    }
                    if($ribInfos->getVille() !== $datasRib->getVille()) {
                        $ribInfos->setVille($datasRib->getVille());
                    }
                    $em->persist($ribInfos);
                    $em->flush();
                    return $this->render('AppBundle:Customer:invoices-saved.html.twig', array(
                        'invoices'=>$tabCharges,
                        'rib_saved'=>1,
                        'form'=>$ribForm->createView()
                    ));
                } else {
                    $rib->setUser($this->getUser());
                    $rib->setIban($datasRib->getIban());
                    $rib->setAdresse($datasRib->getAdresse());
                    $rib->setBic($datasRib->getBic());
                    $rib->setNomTitulaire($datasRib->getNomTitulaire());
                    $rib->setCodePostal($datasRib->getCodePostal());
                    $rib->setVille($datasRib->getVille());
                    $em->persist($rib);
                    $em->flush();

                    /** Si on a pas de rib sauvegardé, on affiche le formulaire vide  */
                    return $this->render('AppBundle:Customer:invoices-saved.html.twig', array(
                        'invoices'=>$tabCharges,
                        'rib_saved'=>1,
                        'form'=>$ribForm->createView()
                    ));
                }
            }else{
                return $this->render('AppBundle:Customer:invoices-saved.html.twig', array(
                    'invoices'=>$tabCharges,
                    'rib_saved'=>0,
                    'form'=>$ribForm->createView()
                ));
            }
        }
      
        return $this->render('AppBundle:Customer:invoices-saved.html.twig', array(
            'form'=>$ribForm->createView()
        ));
    }
    /**
     * @Route("/create-account/{id}", name="create_account")
     */
    public function createAccountAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        if($user->getStripeIdAccount()){
            return $this->render('AppBundle:Customer:create-account.html.twig', array('success'=>2));
        }
        if($request->getMethod() == 'POST'){
            Stripe::setApiKey( $this->container->getParameter('secret_key'));
            $token = $_POST['token'];
            $acct = \Stripe\Account::create(array(
                "country" => "FR",
                "type" => "custom",
                "account_token" => $token,
            ));
            if($user){
                /* On ajoute l'account id  au user concerné */
                $user->setStripeIdAccount($acct->id);
                $em->persist($user);
                $em->flush();
                return $this->render('AppBundle:Customer:create-account.html.twig', array('success'=>1));
            }
        }
        return $this->render('AppBundle:Customer:create-account.html.twig', array(
        ));
    }    
    /**
     * @Route("/security", name="security")
     */
  public function securityAction()
  {
      return $this->render('AppBundle:Customer:security.html.twig');
  }
    
}
