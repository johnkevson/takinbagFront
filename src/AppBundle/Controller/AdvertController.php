<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advert;
use AppBundle\Entity\Message;
use AppBundle\Entity\Paiement;
use AppBundle\Entity\Proposition;
use AppBundle\Form\AdvertType;
use AppBundle\Form\MessageType;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Customer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Advert controller.
 *
 * @Route("advert")
 */
class AdvertController extends Controller
{
    /**
     * Lists all advert entities.
     *
     * @Route("/", name="advert_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $adverts_envoi = $em->getRepository('AppBundle:Advert')->findBy(
            array(
                    'user'=>$this->getUser()->getId(),
                    'typeAnnonce'=>1
            )
        );
        $adverts_transport = $em->getRepository('AppBundle:Advert')->findBy(
            array(
                'user'=>$this->getUser()->getId(),
                'typeAnnonce'=>2
            )
        );

        return $this->render('AppBundle:advert:index.html.twig', array(
            'adverts_envoi' => $adverts_envoi,
            'adverts_transport' => $adverts_transport,
        ));
    }

    /**
     * Creates a new advert entity.
     *
     * @Route("/new", name="advert_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $advert = new Advert();
        $form = $this->createForm(AdvertType::class, $advert, ['action'=>$this->generateUrl('advert_new'),'method'=>'POST']);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $advert->setStatutPublication(1);
            if($this->getUser()){
               $advert->setUser($this->getUser()); 
            }
            $advert->setCreated(new \DateTime());
            $advert->setDatemaj(new \DateTime());
            $em->persist($advert);
            $em->flush();
            return $this->redirectToRoute('advert_show_logout', array('id' => $advert->getId()));
        }else{
            /** Petite variable pour débugger les erreurs du formulaire  **/
            $erreurs = (string) $form->getErrors(true, false);
            return $this->render('AppBundle:advert:new.html.twig', array(
                'advert' => $advert,
                'erreurs'=>$erreurs,
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * Finds and displays a advert entity.
     *
     * @Route("/{id}", name="advert_show")
     * @Method("GET")
     */
    public function showAction(Advert $advert)
    {
        $deleteForm = $this->createDeleteForm($advert);

        return $this->render('AppBundle:advert:show.html.twig', array(
            'advert' => $advert,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a advert entity.
     *
     * @Route("/signaler", name="advert_signaler")
     * @Method("POST")
     */
    public function signalerAction(Request $request)
    {
        if($request->getMethod() == 'POST'){
            if(!empty($request->get('raison_signal')))
            {
                $motif_signal = $request->get('raison_signal');
                $id = $request->get('advert_a_signaler');
                $em = $this->getDoctrine()->getManager();
                $ads = $em->getRepository('AppBundle:Advert')->find($id);
                $ads->setMention($motif_signal);
                $em->flush();
                return $this->render(':default:index.html.twig');
            }
        }
    }

    /**
     * Finds and displays a advert entity in unlogged mode.
     *
     * @Route("/logout/{id}", name="advert_show_logout")
     * @Method("GET")
     */
    public function showLogoutAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager();
        $advert = $repo->getRepository('AppBundle:Advert')->find($id);
        // On envoie les messages
        // On créer le formulaire 
        $messenger = new Message();
        $formBuilder = $this->get('form.factory')->create(MessageType::class, $messenger);
        $formBuilder->handleRequest($request);
        

        if($formBuilder->isSubmitted() && $formBuilder->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($messenger);
            $em->flush();
            return $this->render('AppBundle:advert:show-unlogged.html.twig', array('advert'=>$advert, 'message'=>$messenger, 'form'=>$formBuilder->createView()));
        }
        return $this->render('AppBundle:advert:show-unlogged.html.twig', array(
            'form'=>$formBuilder->createView(),
            'advert' => $advert
        ));
    }

    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/calcul-invoice/{id}", name="advert_invoice_calcul")
     */
    public function calculInvoiceAction(Request $request, $id){
        /* On retourne le prix total en fonction du poids choisi */
        $repo = $this->getDoctrine()->getManager();
        $advert = $repo->getRepository('AppBundle:Advert')->find($id);
        /* On récupère le nombre de kilogrammes et on avance */
        $nbKilosReserves = $request->request->get('nbKilos');
        $prixAuKilo = $advert->getPricePerKilo();
        $prixTotal = $nbKilosReserves*$prixAuKilo;
        return $this->render('AppBundle:advert:form-invoice.html.twig', array('price'=>$prixTotal,'prixAuKg'=>$prixAuKilo,'nbKilos'=>$nbKilosReserves));
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/annonces-recues/{id_user}", name="annonces_recues")
     */
    public function annoncesRecuesAction($id_user){
        // On récupère toutes les lignes dans propositions avec l'id_giver égal à l'id_user courant
        $em = $this->getDoctrine()->getManager();
        $propositions_recues = $em->getRepository('AppBundle:Proposition')->findBy(
            array(
                'idGiver'=>$id_user
            )
        );
        $listeAnnonces = array();
       
        // A partir de l'id de l'user on récupère le descriptif,le prix au kg et le nom du transporteur
        foreach ($propositions_recues as $prop){
            $id_annonce_taker = $prop->getIdAnnonceTaker();
            $tab_propositions = new \stdClass();
            $advert = $em->getRepository('AppBundle:Advert')->find($id_annonce_taker);
            $tab_propositions->prop=$prop;
            $tab_propositions->advert=$advert;
            $listeAnnonces[]=$tab_propositions;
        }
        return $this->render('AppBundle:advert:propositions-recues.html.twig', array(
            'tabPropositions'=>$listeAnnonces
        ));
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/annonces-envoyees/{id_user}", name="annonces_envoyees")
     */
    public function annoncesEnvoyeesAction($id_user){
        $em = $this->getDoctrine()->getManager();
        $tabPropEnvoyees = array();
        $listesPropales = $em->getRepository('AppBundle:Proposition')->findAll();
        foreach($listesPropales as $propale){
            $id_advert_taker = $propale->getIdAnnonceTaker();
            $advert_temp = $em->getRepository('AppBundle:Advert')->find($id_advert_taker); 
            $taker_id = $advert_temp->getUser()->getId();
            if($this->getUser()->getId() === $taker_id){
                $tabPropEnvoyees[] = $advert_temp;
            }
        }
        return $this->render('AppBundle:advert:propositions-envoyees.html.twig', array(
            'tabEnvoyees'=>$tabPropEnvoyees
        ));
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/envoyer-kilos/{id_annonce_envoyee}/{id_annonce_giver}/{poids_requis}", name="envoyer_kilos")
     */
    public function envoyerKilosAction($id_annonce_envoyee, $id_annonce_giver, $poids_requis){
        $em = $this->getDoctrine()->getManager();
        $advert = $em->getRepository('AppBundle:Advert')->find($id_annonce_giver);
        // On récupère l'id du giver
        $id_giver = $advert->getUser()->getId();
        $propositionKilos = new Proposition();
        $propositionKilos->setIdAnnonceGiver($id_annonce_giver);
        $propositionKilos->setIdAnnonceTaker($id_annonce_envoyee);
        $propositionKilos->setPoidsRequis($poids_requis);
        $propositionKilos->setTypeProposition(1);
        $propositionKilos->setidGiver($id_giver);
        $em->persist($propositionKilos);
        $em->flush();
        return $this->render('AppBundle:advert:show-unlogged.html.twig', array(
            'proposition'=>$propositionKilos,
                'advert'=>$advert
            )
        );
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/proposer-kilos/{advert_id}/{user_id}/{departure}/{arrival}/{poids_requis}", name="proposer_kilos")
     */
    public function proposerKilosAction($advert_id,$user_id, $departure, $arrival, $poids_requis){
       $em = $this->getDoctrine()->getManager();
       $adverts_takers = $em->getRepository('AppBundle:Advert')->createQueryBuilder('a')
         ->where('a.user = :user_id')
         ->andWhere('a.typeAnnonce = 1')
         ->andWhere('a.departure LIKE :depart')
         ->andWhere('a.arrival LIKE :arrivee')
         ->setparameter('user_id', $user_id)
         ->setparameter('depart', $departure)
         ->setparameter('arrivee', $arrival)
         ->getQuery()
         ->getArrayResult();
       
       $new_adverts = array();
       foreach($adverts_takers as $ad){
            if($ad['nbKilos'] >= $poids_requis){
                $new_adverts[] = $ad;
            }       
       }
       $nb_adverts = count($new_adverts);
       /* On checke si l'utilisateur a une annonce qui correspond */
       return $this->render('AppBundle:advert:propositions-kilos.html.twig', array(
           'adverts'=>$new_adverts,
           'id_advert_giver'=>$advert_id,
           'user_id'=>$user_id, 
           'departure'=>$departure, 
           'arrival'=>$arrival,
           'poids_requis'=>$poids_requis,
           'nbAdverts'=>$nb_adverts
       ));
       
    }
    /**
     * On procède au paiement de la transaction avec Stripe
     * Et on met à jour les infos dans l'espace client de l'utilisateur payeur et payant
     * @Route("/invoice/{id}", name="advert_invoice")
     */
    public function invoicePaymentAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getManager();
        $advert = $repo->getRepository('AppBundle:Advert')->find($id);
        $messenger = new Message();
        $formBuilder = $this->get('form.factory')->create(MessageType::class, $messenger);
        $formBuilder->handleRequest($request);
        if($request->getMethod()=='POST'){
            $stripe = array(
                "secret_key" => $this->container->getParameter('secret_key'),
                "publishable_key" => $this->container->getParameter('public_key')
            );

            $token  = $request->request->get('stripeToken');
            $emailCustomer  = $request->request->get('stripeEmail');
            // Si l'email de celui qui est connecté est différent de l'email renseigné on refuse le paiement
            if($emailCustomer != $this->getUser()->getEmail()){
                return $this->render('AppBundle:advert:show-unlogged.html.twig', array(
                        'advert'=>$advert,
                        'form'=>$formBuilder->createView(),
                        'emailFaux'=>1
                    )
                );
            }
            Stripe::setApiKey($stripe['secret_key']);
                        $customer = Customer::create(array(
                'email' => $emailCustomer,
                'source'  => $token
            ));
            $nbKilosReserves = $request->request->get('nbKilosReserves');
            $prixAuKilo = $advert->getPricePerKilo();
            $totalACharger = $nbKilosReserves*$prixAuKilo*100;
            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount'   => "$totalACharger",
                'currency' => 'eur'
            ));
            if($charge){
                /* On enlève le nombre de kilos à l'annonce en question */
                $nbKilosRestants = $advert->getNbKilos()-$nbKilosReserves;
                $advert->setNbKilos($nbKilosRestants);
                if($nbKilosRestants == 0){
                    /* On désactive l'annonce, sinon on met à jour le nombre de kilos */
                    $advert->setStatutPublication(0);
                }
                $repo->flush();
                /* On gère l'ajout dans la table des paiements */
                $p = new Paiement();
                /* On calcule le montant à envoyer en retirant ce qu'on paye à stripe 1.4% +0.25 et notre commission 2% */
                $comissionStripe = (($totalACharger*2.9)/100)+0.25;
                $comissionTk = (($totalACharger*2.5)/100);
                $fraisTk = $comissionStripe+$comissionTk;
                $gainTaker = round($totalACharger - $fraisTk,2);
                // On récupère l'objet du customer égal au taker à payer
                $emailCustomerTaker = $advert->getUser()->getEmail();
                $emailCustomerGiver = $this->getUser()->getEmail();
                $timestamp = $charge->created;
                /* On remplit le paiement */
                $charge = $charge->__toArray(TRUE);
                $p->setMontantCalcule(round($gainTaker/100,2));
                $p->setDatePaiement(new \DateTime("@$timestamp"));
                $p->setDateMaj(new \DateTime('now'));
                $p->setAuteurPaiement($emailCustomerGiver);
                $p->setDestinatairePaiement($emailCustomerTaker);
                $p->setMontantInitial(round($totalACharger/100,2));
                $p->setNumPaiement($charge['id']);
                $repo->persist($p);
                $repo->flush();
                return $this->render('AppBundle:advert:show-unlogged.html.twig', array(
                        'advert'=>$advert,
                        'form'=>$formBuilder->createView(),
                        'charge'=>$charge
                    )
                );
                
            }
            
        }
        return $this->render('AppBundle:advert:show-unlogged.html.twig', array(
                'advert'=>$advert,
                'form'=>$formBuilder->createView()
            )
        );
        

    }
  
    /**
     * Displays a form to edit an existing advert entity.
     *
     * @Route("/{id}/edit", name="advert_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Advert $advert)
    {

        if (!$advert->isAuthor($this->getUser())) {
            $this->denyAccessUnlessGranted('edit', $advert);
        }
        $deleteForm = $this->createDeleteForm($advert);

        $editForm = $this->createForm('AppBundle\Form\AdvertType', $advert);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('advert_edit', array('id' => $advert->getId()));
        }

        return $this->render('AppBundle:advert:edit.html.twig', array(
            'advert' => $advert,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a advert entity.
     *
     * @Route("/{id}", name="advert_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Advert $advert)
    {
        $form = $this->createDeleteForm($advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($advert);
            $em->flush();
        }

        return $this->redirectToRoute('advert_index');
    }

    /**
     * Creates a form to delete a advert entity.
     *
     * @param Advert $advert The advert entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Advert $advert)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('advert_delete', array('id' => $advert->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
