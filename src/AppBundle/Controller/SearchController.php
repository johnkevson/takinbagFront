<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Forms;

/**
 * Search controller.
 *
 * @Route("search")
 */
class SearchController extends Controller
{
    /**
     * Lists all adverts.
     *
     * @Route("/", name="search_home")
     */
    public function searchHomeAction(Request $request)
    {
        $formFactory = Forms::createFormFactory();
        $form = $formFactory->createBuilder()
            ->add('first_place', TextType::class, array('required'=>false))
            ->add('end_place', TextType::class, array('required'=>false))
            ->add('search_date', TextType::class, array('required'=>false))
            ->add('tri_date', ChoiceType::class, array(
                'required'=>false,
                'choices'=>array(
                    'Tri par date'=>'',
                    'Croissant'=>'100',
                    'Décroissant'=>'101'
                ),
                'label'=>false
            ))
            ->add('tri_type', ChoiceType::class, array(
                'required'=>false,
                'choices'=>array(
                    'Tri par type'=>'',
                    'Transport de colis'=>'110',
                    'Envoi de colis'=>'111'
                ),
                'label'=>false
            ))
            ->add('tri_price', ChoiceType::class, array(
                'required'=>false,
                'choices'=>array(
                    'Tri par prix'=>'',
                    'Croissant'=>'1000',
                    'Décroissant'=>'1001'
                ),
                'label'=>false
                ))
            ->add('tri_way', ChoiceType::class, array(
                'required'=>false,
                'choices'=>array(
                    'Tri par voie'=>'',
                    'Avion'=>'1010',
                    'Train'=>'1011',
                    'Voiture'=>'1100',
                    'Bateau'=>'1101'
                ),
                'label'=>false
            ))
            ->getForm();
        $form->handleRequest();
        if($request->isXmlHttpRequest()){
            $this->searchResultAction($request, 1);
        }
        return $this->render('AppBundle:search:index.html.twig', array('search'=>$form->createView()));
    }
    
    /**
     * Lists all adverts with pagination
     *
     * @Route("/{page}", name="search_result",  requirements={"page" = "\d+"}, defaults={"page"= 1})
     */
    public function searchResultAction(Request $request, $page)
    {
        
        $nbArticlesParPage = $this->container->getParameter('front_nb_articles_par_page');
        // On récupère le mot clé à rechercher
        $tokens = $request->get('form');
        // On recherche les données dans notre table 
        $em = $this->getDoctrine()->getManager();
      
        // On a un token de départ et d'arrivée, sinon on en a pas et on affiche tout
        if(!empty($tokens['first_place']) && !empty($tokens['end_place'])){
           // On apelle la fonction findmyAdverts dans le repository
            $repo = $em->getRepository('AppBundle:Advert');
            $adverts = $repo->matchAdverts($tokens['first_place'],$tokens['end_place'],$tokens['search_date'], $page, $nbArticlesParPage, $tokens['tri_date'],$tokens['tri_price'],$tokens['tri_type'], $tokens['tri_way']);
            if($adverts == null){
                return $this->render('AppBundle:search:list.html.twig', array('advert'=>null));
            }
            $pagination = array(
                'page' => $page,
                'nbPages' => ceil(count($adverts) / $nbArticlesParPage),
                'nomRoute' => 'search_result',
                'paramsRoute' => array()
            );
            return $this->render('AppBundle:search:list.html.twig', array('adverts'=>$adverts, 'pagination'=>$pagination));
        } else {
            $adverts = isset($tokens) ? $em->getRepository('AppBundle:Advert')->getAdverts($page, $nbArticlesParPage, $tokens['tri_date'],$tokens['tri_price'],$tokens['tri_type'], $tokens['tri_way']) : $em->getRepository('AppBundle:Advert')->getAdverts($page, $nbArticlesParPage, null, null, null, null);
            $pagination = array(
                'page' => $page,
                'nbPages' => ceil(count($adverts) / $nbArticlesParPage),
                'nomRoute' => 'search_result',
                'paramsRoute' => array()
            );
            return $this->render('AppBundle:search:list.html.twig',array('adverts'=>$adverts, 'pagination'=>$pagination));
        }
    }
    

  
}
