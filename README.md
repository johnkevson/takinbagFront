Takinbag - Plateforme de livraison de colis entre particuliers 
========

![Takinbag](docs/images/logo.png)

## Documentation
La documentation est disponible [ici](docs/index.md).

## Changelog
[CHANGELOG.md](changelog.md) liste de tous les changements effectués lors de chaque release.

## A propos
Takinbag a été conçu par Zicklr (Kevin Minkol).
Si vous avez la moindre question, contactez [Kevin Minkol](mailto:johnkevson@gmail.com)