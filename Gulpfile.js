var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    prefix =  require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    bs = require('browser-sync').create();

// paths
var resources = './web/';
var sassDir = resources+'public/sass/';

gulp.task('sass', function(){
    return gulp.src(
        sassDir+'main.scss'
    )
    .pipe(sass(
        {
            outputStyle:'nested'
        }
    ).on('error', sass.logError))

    .pipe(cssmin())
    .pipe(prefix('last 2 versions'))
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('web/public/css'))
    .pipe(bs.reload({stream:true}));
});
gulp.task('js', function(){
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/@chenfengyuan/datepicker/dist/datepicker.min.js',
        'node_modules/@chenfengyuan/datepicker/i18n/datepicker.fr-FR.js',
        'node_modules/google-maps/lib/Google.min.js',
        'web/public/js/autocomplete.js',
        'node_modules/uikit/dist/js/uikit.min.js',
        'node_modules/uikit/dist/js/uikit-icons.min.js'
    ])
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('web/public/js'))
        .pipe(bs.reload({stream:true}));
});

gulp.task('images', function(){
    return gulp.src('web/public/img/*.+(png|jpg|gif|svg)')
        .pipe(imagemin())
        .pipe(gulp.dest('web/public/img'));
});


gulp.task('watch', function() {
    gulp.watch('./web/public/sass/*.scss', gulp.series('sass'));
    gulp.watch('./web/public/js/autocomplete.js', gulp.series('js'));
    gulp.watch('./web/public/img/*', gulp.series('images'));
});
gulp.task('default', gulp.series('sass','js', 'images', 'watch'));