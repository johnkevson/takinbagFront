GoogleMapsLoader.KEY = 'AIzaSyBKTi8NlfLgYeCf7enzS3V0TVE3joZmV7E';
GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
GoogleMapsLoader.LANGUAGE = 'fr';
GoogleMapsLoader.version='3.14';

GoogleMapsLoader.load(function(google) {
    function initializeAutocomplete(id) {
        var element = document.getElementById(id);
        if (element) {
            var autocomplete = new google.maps.places.Autocomplete(element, { types: ['geocode'] });
            google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
        }
    }

    function onPlaceChanged() {
        var place = this.getPlace();

        for (var i in place.address_components) {
            var component = place.address_components[i];
            for (var j in component.types) {  // Some types are ["country", "political"]
                var type_element = document.getElementById(component.types[j]);
                if (type_element) {
                    type_element.value = component.long_name;
                }
            }
        }
    }

    google.maps.event.addDomListener(window, 'click', function() {
        initializeAutocomplete('form_first_place');
        initializeAutocomplete('form_end_place');
        initializeAutocomplete('appbundle_advert_departure');
        initializeAutocomplete('appbundle_advert_arrival');
    });
});

$('[data-toggle="datepicker"]').datepicker({
    language:'fr-FR',
    startDate: new Date(),
    format:'yyyy-mm-dd',
    autoHide:true
});
$(document).ready(function(){
    $('#rechercheAdvert').submit(function(){
        $('#spinn').removeClass("uk-invisible");
        $("#spinn").fadeIn(3000);
        // On lance
        $.ajax({
            'type':'POST',
            'url': "/search/1",
            'data':$(this).serialize(),
            'beforeSend':function () {
                $("#spinn").removeClass("uk-invisible");
            },
            'success': function(data) {
                $("#resultatsRecherche").html(data);
            },
            'complete':function(){
                $("#spinn").addClass("uk-invisible");
                $("#filter-search").removeClass("uk-invisible");
            }
        });
        return false;
    });

    /* Tri par date, filtre de recherche */
    $(".triParDate").on('change', function () {
        $.ajax({
            'type':'POST',
            'url':"/search/1",
            'data': $("form").serialize(),
            'success':function (data) {
                $("#resultatsRecherche").html(data);
            }
        });
    });

});
$('#appbundle_advert_typeAnnonce').on('change', function(){
    var typeAnnonce = $(this).val();
    if(typeAnnonce == '2'){
        $('.champEnvois').removeClass('uk-hidden');
        $('.champTaker').addClass('uk-hidden');

    }
    if(typeAnnonce == '1'){
        $('.champEnvois').addClass('uk-hidden');
        $('#appbundle_advert_dateArrival').removeAttr('required');
        $('.champTaker').removeClass('uk-hidden');
    }

});

/* Fonction signaler */
$('.signaler').on('click', function(){
    var advertId = $(this).attr('data-id');
    $("input[name='advert_a_signaler']").attr("value", advertId);
    return false;
});
/* Rechargement de la liste des annonces en fonction du numéro de la page demandée */
$(document).on('click','.ajax-pagination', function(){
    // On lance
    $.ajax({
        'type':'POST',
        'url': $(this).attr("href"),
        'success': function(data) {
            $("#resultatsRecherche").html(data);
        }
    });
    return false;
});


