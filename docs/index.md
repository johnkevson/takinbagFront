#Documentation
##Sommaire
[1. Description](1_description.md)

[2. Note de cadrage](2_note_cadrage.md)

[3. Installation](3_installation.md)

[4. Environnements](4_environnements.md)

[5. Intégration continue](5_integration_continue.md)

