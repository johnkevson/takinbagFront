#Description du projet
[Retour au sommaire](index.md)

## Première expression du besoin
```text
Contexte:
L'idée était de créer une plateforme de livraison de colis entre particuliers.
La plateforme permet la mise en relation entre des emeteurs de colis et les transporteurs particuliers qui ont des kilos en trop.
Le Sender, recherche un trajet d'un transporteur dans notre moteur de recherche, s'il  soit il poste une nouvelle annonce de prise en charge de colis.  
Prérequis :
- Gestion et affichage d’une boutique de lots
    - UX performant et responsive
    - Critères de filtre et de recherche multiple
- Rôle et privilèges d’accès : Admin LADR, Admin Groupement, Admin Indépendant, Commercial Adhérent, Garagiste-Carrossier, Collaborateur Adhérent
- Accès Backoffice pour gestion CRUD des bénéficiaires ; pour les accès : Admin LADR, Admin Groupement, Admin Indépendant
- Accès Backoffice pour paramétrage d’affichage des boutiques selon typologie des adhérents ; uniquement pour l’accès Admin LADR
- Gestion des achats de clés et du suivi de facturation
- Gestion des mouvements de clés : crédit, débit, retrait
- Suivi logistique des commandes de lots
- Ajouter la possibilité pour chaque bénéficiaire d’un lot de déclencher une demande de SAV
- A date, plusieurs des fonctionnalités ci-dessus ont été développées mais ne sont pas administrables et consultables facilement. C’est un casse-tête d’ajouter un adhérent par exemple. C’est également impossible de suivre facilement les mouvements de clés pour un admin.
```
